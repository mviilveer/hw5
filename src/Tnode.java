import java.util.*;

public class Tnode {

    private String name;
    private Tnode firstChild;
    private Tnode nextSibling;

    @Override
    public String toString() {
        StringBuffer b = new StringBuffer();
        b.append(name);
        if (getDoubleValue(name) == null) {
            if (firstChild != null) {
                b.append("(");
                b.append(firstChild.toString());
                b.append(")");
            }
            if (nextSibling != null) {
                b.append(",");
                b.append(nextSibling.toString());
            }
        } else {
            if (nextSibling != null) {
                b.append(",");
                b.append(nextSibling.toString());
            }
            if (firstChild != null) {
                b.append("(");
                b.append(firstChild.toString());
                b.append(")");
            }
        }
        return b.toString();
    }

    public Tnode(String name) {
        this.name = name;
    }

    public Tnode(String name, Tnode nextSibling) {
        this.name = name;
        this.nextSibling = nextSibling;
    }

    public Tnode(String name, Tnode firstChild, Tnode nextSibling) {
        this.name = name;
        this.firstChild = firstChild;
        this.nextSibling = nextSibling;
    }

    public void addNextSibling(Tnode nextSibling) {
        this.nextSibling = nextSibling;
    }

    public static Tnode buildFromRPN(String pol) {
        if (pol.isEmpty()) {
            throw new RuntimeException("RPN is empty.");
        }
        String[] parts = pol.trim().split(" ");

        try {
            validateRPN(parts);

        } catch (RuntimeException e) {
            throw new RuntimeException("Cannot validate RPN: \"" + pol + "\": " + e.getMessage());
        }
        Tnode lastNode = new Tnode(parts[0]);
        for (int i = 1; i < parts.length; i++) {
            if (isArithmeticNodePossible(parts, i)) {
                lastNode.addNextSibling(getArithmeticTNode(parts, i));
                i = i + 2;
                continue;
            }

            if (getDoubleValue(parts[i]) != null) {
                lastNode.addNextSibling(new Tnode(parts[i]));
            } else {
                lastNode = new Tnode(parts[i], lastNode, null);
            }

        }

        return lastNode;
    }

    private static void validateRPN(String[] parts) {
        if (parts.length == 0) {
            throw new RuntimeException("RPN is empty.");
        }

        int numericalValues = 0;
        for (String part : parts) {
            if (getDoubleValue(part) != null) {
                numericalValues++;
            }
        }
        if (numericalValues < 2 && parts.length - numericalValues != 0) {
            throw new RuntimeException("RPN must contain atleast two numbers.");
        }

        int counter = 0;
        for (String part : parts) {
            if (getDoubleValue(part) != null) {
                counter++;
            } else if (Arrays.asList("*", "/", "-", "+").contains(part)) {
                counter = counter - 2;
                if (counter < 0) {
                    throw new RuntimeException("Malformed RPN. Arithmetic sign is positioned wrong.");
                }
                counter++;
            } else {
                throw new RuntimeException("RPN Expression part '" + part + "' is invalid");
            }
        }
        if (counter > 1) {
            throw new RuntimeException("Malformed RPN. Too few arithmetic signs.");
        }
        if (counter < 1) {
            throw new RuntimeException("Malformed RPN. Too many arithmetic signs.");
        }
    }

    public static boolean isArithmeticNodePossible(String[] parts, int i) {
        if (i <= parts.length && i + 1 <= parts.length && i + 2 <= parts.length) {
            return getDoubleValue(parts[i]) != null &&
                    getDoubleValue(parts[i + 1]) != null &&
                    getDoubleValue(parts[i + 2]) == null;
        }
        return false;
    }

    public static Tnode getArithmeticTNode(String[] parts, int i) {
        Tnode number2 = new Tnode(parts[i + 1]);
        Tnode number1 = new Tnode(parts[i], number2);
        return new Tnode(parts[i + 2], number1, null);
    }


    public static void main(String[] param) {
        String rpn = "";
        System.out.println("RPN: " + rpn);
        Tnode res = buildFromRPN(rpn);
        System.out.println("Tree: " + res);
        // TODO!!! Your tests here
    }


    public static Double getDoubleValue(String number) {
        try {
            return Double.parseDouble(number);
        } catch (NumberFormatException nfe) {
            return null;
        }
    }
}

